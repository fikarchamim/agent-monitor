import psutil
import shutil
import platform

def get_system_info():
    system_info = {
        'System': platform.system(),
        'Node Name': platform.node(),
        'Release': platform.release(),
        'Version': platform.version(),
        'Machine': platform.machine(),
        'Processor': platform.processor(),
    }
    return system_info

def bytes_to_megabytes(bytes_value):
    return bytes_value / (1024 ** 2)  # Convert bytes to megabytes

def get_disk_info():
    disk_usage = shutil.disk_usage('/')
    disk_info = {
        'Total Disk Space': bytes_to_megabytes(disk_usage.total),
        'Free Disk Space': bytes_to_megabytes(disk_usage.free),
        'Used Disk Space': bytes_to_megabytes(disk_usage.used),
        'Disk Usage Percentage': (disk_usage.used / disk_usage.total) * 100
    }
    return disk_info

def get_memory_info():
    memory_info = {
        'Total Memory': bytes_to_megabytes(psutil.virtual_memory().total),
        'Available Memory': bytes_to_megabytes(psutil.virtual_memory().available),
        'Used Memory': bytes_to_megabytes(psutil.virtual_memory().used),
        'Memory Usage Percentage': psutil.virtual_memory().percent
    }
    return memory_info

def get_cpu_info():
    cpu_info = {
        'CPU Usage Percentage': psutil.cpu_percent(interval=1)
    }
    return cpu_info

if __name__ == "__main__":
    disk_info = get_disk_info()
    memory_info = get_memory_info()
    cpu_info = get_cpu_info()

    print("Disk Information:")
    for key, value in disk_info.items():
        print(f"{key}: {value:.2f} MB" if 'Percentage' not in key else f"{key}: {value:.2f}%")

    print("\nMemory Information:")
    for key, value in memory_info.items():
        print(f"{key}: {value:.2f} MB" if 'Percentage' not in key else f"{key}: {value:.2f}%")

    print("\nCPU Information:")
    for key, value in cpu_info.items():
        print(f"{key}: {value:.2f}%")
