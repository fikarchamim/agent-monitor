import os
import sys
sys.path.append(os.getcwd())

from typing import Annotated, Union

from fastapi import FastAPI, Header, HTTPException
from werkzeug.security import check_password_hash, generate_password_hash

from src.platform_details import get_disk_info, get_memory_info, get_cpu_info, get_system_info

PASSWORD = os.getenv('PASSWORD', 'default')
DEV = int(os.getenv('DEV', 0))

app = FastAPI(
    docs_url=None if DEV == 0 else "/docs",
    redoc_url=None if DEV == 0 else "/redocs"
)

def check_api_key(hashed_pass, password):
    try:
        if check_password_hash(hashed_pass, password):
            return True
    except Exception:
        return False


@app.get(
    "/system-properties",
)
def read_item(api_key: Annotated[
        Union[str, None], Header(convert_underscores=False)
    ] = None,
):
    is_correct = check_api_key(api_key, PASSWORD)

    if is_correct:
        system_info = get_system_info()
        disk_info = get_disk_info()
        memory_info = get_memory_info()
        cpu_info = get_cpu_info()

        system_properties = {
            "system_info": system_info,
            "disk_info": {
                'Total Disk Space': f"{disk_info['Total Disk Space']:.2f} MB",
                'Free Disk Space': f"{disk_info['Free Disk Space']:.2f} MB",
                'Used Disk Space': f"{disk_info['Used Disk Space']:.2f} MB",
                'Disk Usage Percentage': f"{disk_info['Disk Usage Percentage']:.2f}%",
            },
            "memory_info": {
                'Total Memory': f"{memory_info['Total Memory']:.2f} MB",
                'Available Memory': f"{memory_info['Available Memory']:.2f} MB",
                'Used Memory': f"{memory_info['Used Memory']:.2f} MB",
                'Memory Usage Percentage': f"{memory_info['Memory Usage Percentage']:.2f}%"
            },
            "cpu_info": {
                'CPU Usage Percentage': f"{cpu_info['CPU Usage Percentage']:.2f}%"
            }
        }

        return system_properties
    
    else:
        raise HTTPException(status_code=401, detail="Unauthorized")

