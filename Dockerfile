FROM python:3.10-slim-bookworm

WORKDIR /home/agent-monitor

RUN apt-get update

COPY ./requirements.txt .
RUN pip install -r requirements.txt

COPY ./src/main.py ./src/
COPY ./src/platform_details.py ./src/

CMD ["uvicorn", "src.main:app", "--host", "0.0.0.0", "--port", "8000"]
