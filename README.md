# Installation


### Docker Run

```shell
sudo docker run -dp 0.0.0.0:40003:8000 --name agent-monitor --env PASSWORD=<password> registry.gitlab.com/fikarchamim/agent-monitor:latest
```

### ENV

```shell
- PASSWORD
```

### Password Hash

```
python3 -c "from werkzeug.security import generate_password_hash; print(generate_password_hash(<password_string>))"
```

