import os
import sys
sys.path.append(os.getcwd())

from fastapi.testclient import TestClient

from src.main import app

client = TestClient(app)

def test_response():
    response = client.get("/system-properties", headers={"api_key": "scrypt:32768:8:1$puYzNLx0pG4lC503$91870e3614edd4e687989951616c4b228f511cd1475b0242517dc697dc62ded706091d008e38841b088d75cbbb467cb094293b10b6203dc46bb841720d9749fb"})
    assert response.status_code == 200


def test_response_unauthorized():
    response = client.get("/system-properties")
    assert response.status_code == 401

